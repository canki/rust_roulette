﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Casino
    {
        private static readonly IReadOnlyList<int> Values = new[]
            {1, 3, 1, 10, 1, 3, 1, 5, 1, 5, 3, 1, 10, 1, 3, 1, 5, 1, 3, 1, 20, 1, 3, 1, 5};

        private Dictionary<int, int> betPlace = new Dictionary<int, int>
        {
            [1] = 0,
            [3] = 1,
            [5] = 2,
            [10] = 3,
            [20] = 4
        };

        private int Spin(out int roll)
        {
            roll = Values.ToList().OrderBy(x => Guid.NewGuid()).First();
            return betPlace[roll];
        }

        public int PlaceBet(out int totalBet, out int rollResult, int bet1 = 0, int bet3 = 0, int bet5 = 0, int bet10 = 0, int bet20 = 0)
        {
            int[] bet = { bet1, bet3, bet5, bet10, bet20 };

            totalBet = bet.Sum();

            var win = bet[Spin(out int roll)];

            rollResult = roll;

            return win * roll + win;
        }
    }
}
