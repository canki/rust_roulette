﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public interface ITactic
    {
        string Name { get; }
        void Setup(ref int bank);
        void Step(ref int bank, Casino casino, IOutput output);
    }
}
