﻿using ConsoleApp1.Tactics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp1
{
    class Program
    {
        private static volatile int gamesCounter = 0;

        private static Func<ITactic>[] tactics = { () => new TP1Tactic(), () => new WorondTactic(), () => new FlashTactic(), () => new HotWhellTactic() };

        static void Main(string[] args)
        {
            var casino = new Casino();

            while (true)
            {
                Console.WriteLine("Choose tactic");
                for (int i = 0; i < tactics.Length; i++)
                {
                    Console.WriteLine($"[{i + 1}] {tactics[i]().Name}");
                }

                int tacticNo;

                while (!int.TryParse(Console.ReadKey(true).KeyChar.ToString(), out tacticNo) || tacticNo > tactics.Length || tacticNo == 0)
                {
                    Console.WriteLine("Incorrect tactic");
                }

                Console.WriteLine($"Choosen tactic - {tactics[tacticNo - 1]().Name}");

                var results = PlayGame(() => new Game(targetBankRate: 3), tactics[tacticNo - 1], 10000, casino);
                var wins = results.Where(r => r.IsWin).ToList();

                Console.WriteLine($"Wins: {wins.Count()}");
                Console.WriteLine($"AvgTime: {TimeSpan.FromSeconds(wins.Average(w => w.Steps) * 40)}");

                results = PlayGame(() => new Game(stepLimit: 45), tactics[tacticNo - 1], 10000, casino);
                wins = results.Where(r => r.IsWin).ToList();
                Console.WriteLine($"Wins: {wins.Count()}");
                Console.WriteLine($"AvgEndBankForWins: {wins.Average(w => w.EndBank)}");

                Application.EnableVisualStyles();
                Application.Run(new Plot(results.Select((g, ind) => new Tuple<int, int>(ind, g.EndBank))) { Text = tactics[tacticNo - 1]().Name });

                var totalSteps = 180;
                var startStep = 3;
                var gameAvgProfitAtStep = new double[totalSteps];
                var gameWinsAtStep = new int[totalSteps];
                for (int step = startStep; step < gameAvgProfitAtStep.Length; step++)
                {
                    var resultsAtStep = PlayGame(() => new Game(stepLimit: step), tactics[tacticNo - 1], 100, casino);
                    gameAvgProfitAtStep[step] = resultsAtStep.Average(r => r.EndBank);
                    gameWinsAtStep[step] = resultsAtStep.Count(r => r.IsWin);
                }

                Application.EnableVisualStyles();
                Application.Run(new Plot(
                    gameAvgProfitAtStep.Skip(startStep).Select((p, ind) => new Tuple<int, double>(ind + startStep, p)),
                    gameWinsAtStep.Skip(startStep).Select((w, ind) => new Tuple<int, int>(ind + startStep, w))
                    )
                { Text = tactics[tacticNo - 1]().Name + " by step" });
            }
        }

        static List<GameResult> PlayGame(Func<Game> gameF, Func<ITactic> tacticF, int gamesCount, Casino casino)
        {
            var results = new List<GameResult>(gamesCount);

            gamesCounter = 0;

            void Play(Game game, ITactic tactic, int count)
            {
                for (int i = 0; i < count; i++)
                {
                    results.Add(game.Play(tactic, casino, silent: true));
                    gamesCounter++;
                    Console.Write($"\rGames played {gamesCounter:D4}");
                }
            }

            int threadsCount = 4;
            var tasks = new Task[threadsCount];
            for (int i = 0; i < threadsCount; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    var game = gameF();
                    var tactic = tacticF();
                    Play(game, tactic, gamesCount / threadsCount);
                });
            }

            Task.WaitAll(tasks);

            Console.WriteLine();

            return results;
        }
    }
}
