﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public struct Game
    {
        private int _targetBankRate;
        private int _stepLimit;

        public Game(int targetBankRate = 0, int stepLimit = 0)
        {
            _targetBankRate = targetBankRate;
            _stepLimit = stepLimit;
        }

        public GameResult Play(ITactic tactic, Casino casino, bool silent = false)
        {
            IOutput @out = silent ? (IOutput)new EmptyOutput() : new ConsoleOutput();

            int bank = 0;

            tactic.Setup(ref bank);

            if (bank == 0)
                throw new InvalidOperationException("Bank is empty");

            int startBank = bank;
            int targetBank = bank * _targetBankRate;

            @out.WriteLine($"Play game with {tactic.Name} tactic, start bank: {bank}");
            int step = 1;
            while (bank > 0)
            {
                @out.WriteLine($"step {step} start---");
                tactic.Step(ref bank, casino, @out);

                if (targetBank > 0 && bank > targetBank)
                {
                    @out.WriteLine("You win!");
                    break;
                }
                if (_stepLimit > 0 && step == _stepLimit)
                {
                    break;
                }
                @out.WriteLine($"step {step} end--- Time spent {TimeSpan.FromSeconds(40 * step)}");
                step++;
            }

            if (bank <= 0)
                @out.WriteLine("You looooose!! Tu du du duuu");

            return new GameResult
            {
                StartBank = startBank,
                TargetBank = targetBank,
                EndBank = bank,
                Steps = step
            };
        }
    }
}
