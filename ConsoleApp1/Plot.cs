﻿using OxyPlot;
using OxyPlot.Series;
using OxyPlot.WindowsForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp1
{
    public partial class Plot : Form
    {
        public Plot(IEnumerable<Tuple<int,int>> data)
        {
            InitializeComponent();

            //Create Plotview object
            PlotView myPlot = new PlotView();

            //Create Plotmodel object
            var myModel = new PlotModel { Title = "Simple Plot" };
            myModel.Series.Add(new ScatterSeries() { ItemsSource = data.Select(d => new ScatterPoint(d.Item1, d.Item2, 2)).ToList() });

            //Assign PlotModel to PlotView
            myPlot.Model = myModel;

            //Set up plot for display
            myPlot.Dock = System.Windows.Forms.DockStyle.Bottom;
            myPlot.Location = new System.Drawing.Point(0, 0);
            myPlot.Size = new System.Drawing.Size(500, 500);
            myPlot.TabIndex = 0;

            //Add plot control to form
            Controls.Add(myPlot);
        }

        public Plot(IEnumerable<Tuple<int, double>> profitAtStep, IEnumerable<Tuple<int,int>> winsAtStep)
        {
            InitializeComponent();

            //Create Plotview object
            PlotView myPlot = new PlotView();

            //Create Plotmodel object
            var myModel = new PlotModel { Title = "Simple Plot" };
            myModel.Series.Add(new LineSeries() { ItemsSource = profitAtStep.Select(d => new DataPoint(d.Item1, d.Item2)).ToList() });
            myModel.Series.Add(new LineSeries() { ItemsSource = winsAtStep.Select(d => new DataPoint(d.Item1, d.Item2*10)).ToList() });
            //Assign PlotModel to PlotView
            myPlot.Model = myModel;

            //Set up plot for display
            myPlot.Dock = System.Windows.Forms.DockStyle.Bottom;
            myPlot.Location = new System.Drawing.Point(0, 0);
            myPlot.Size = new System.Drawing.Size(500, 500);
            myPlot.TabIndex = 0;

            //Add plot control to form
            Controls.Add(myPlot);
        }
    }
}
