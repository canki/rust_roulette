﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public interface IOutput
    {
        void WriteLine(string text);
    }

    public struct EmptyOutput : IOutput
    {
        public void WriteLine(string text)
        {
        }
    }


    public struct ConsoleOutput :IOutput
    {
        public void WriteLine(string text) => Console.WriteLine(text);
    }
}
