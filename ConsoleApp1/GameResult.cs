﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public struct GameResult
    {
        public int StartBank { get; set; }
        public int TargetBank { get; set; }
        public int EndBank { get; set; }
        public bool IsWin => EndBank > TargetBank;
        public int Steps { get; set; }
    }
}
