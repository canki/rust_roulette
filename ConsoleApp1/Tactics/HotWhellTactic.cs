﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tactics
{
    public struct HotWhellTactic : ITactic
    {
        public string Name => "1-20";

        private int[] _rate1steps ;
        private int[] _rate20steps;

        private int _rate1;
        private int _rate20;

        private int _step;

        public void Setup(ref int bank)
        {
            bank = 720;

            _step = 0;

            _rate1steps = new[] { 1, 4, 12, 30, 70, 160, 360 };
            _rate20steps = new[] { 1, 2, 4, 6, 10, 20, 40 };

            _rate1 = _rate1steps[_step];
            _rate20 = _rate20steps[_step];
        }

        public void Step(ref int bank, Casino casino, IOutput output)
        {
            var gain = casino.PlaceBet(out int totalBet, out int roll, bet1: _rate1, bet20: _rate20);

            if (gain == 0)
            {
                bank -= totalBet;
                if (_step < 6)
                {
                    _step++;
                    _rate1 = _rate1steps[_step];
                    _rate20 = _rate20steps[_step];
                }
            }
            else
            {
                bank += gain;
                _step = 0;
                _rate1 = _rate1steps[_step];
                _rate20 = _rate20steps[_step];
            }
        }
    }
}
