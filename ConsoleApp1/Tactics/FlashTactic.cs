﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tactics
{
    public struct FlashTactic : ITactic
    {
        public string Name => "Flash";

        private int _startRate;
        private int _rate;
        private int _step;

        public void Setup(ref int bank)
        {
            bank = 635;
            _startRate = 5;
            _rate = _startRate;
            _step = 0;
        }

        public void Step(ref int bank, Casino casino, IOutput @out)
        {
            var gain = casino.PlaceBet(out int totalBet, out int roll, bet1: _rate);
            if (gain == 0)
            {
                bank -= totalBet;
                _step++;
                _rate *= 2;
                @out.WriteLine($"raise to {_rate}. Tactic step: {_step}");
            }
            else
            {
                _step = 0;
                bank = bank + gain;
                if (bank - (_startRate + 1) * 64 > 0)
                {
                    _startRate++;
                    @out.WriteLine($"Raise start bet to {_startRate}");
                }
                _rate = _startRate;
                @out.WriteLine($"Win +{gain}. Bank: {bank}");
            }

        }

    }
}
