﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tactics
{
    public struct WorondTactic : ITactic
    {
        public string Name => "Worond";

        private int _rate;
        private int _step;

        public void Setup(ref int bank)
        {
            bank = 378;

            _rate = 2;
            _step = 0;
        }

        public void Step(ref int bank, Casino casino, IOutput @out)
        {
            var gain = casino.PlaceBet(out int totalBet, out int roll, bet5: _rate, bet10: _rate, bet20: _rate);
            if (gain == 0)
            {
                bank -= totalBet;
                if (_step % 2 == 1)
                {
                    _rate *= 2;
                    @out.WriteLine($"raise to {_rate}. Tactic step: {_step}");
                }
                _step++;
            }
            else
            {
                _step = 0;
                _rate = 2;
                bank = bank + gain;
                @out.WriteLine($"Win +{gain}. Bank: {bank}");
            }
        }
    }
}
