﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tactics
{
    public struct TP1Tactic : ITactic
    {
        public string Name => "TP1";

        private int _rate10;
        private int _rate20;

        private int _currentLoss;

        public void Setup(ref int bank)
        {
            bank = 300;

            _rate10 = 6;
            _rate20 = 3;

            _currentLoss = 0;
        }

        public void Step(ref int bank, Casino casino, IOutput @out)
        {
            var gain = casino.PlaceBet(out int totalBet, out int roll, bet10: _rate10, bet20: _rate20);
            if (gain == 0)
            {
                _currentLoss += totalBet;
                bank -= totalBet;
                if (bank - totalBet * 10 < 0)
                {
                    _rate10 -= 2;
                    _rate20 -= 1;
                    @out.WriteLine($"hold down to {_rate10} {_rate20} Bank: {bank}");
                    return;
                }
            }
            else
            {
                bank = bank + gain;
                _currentLoss -= gain;
                @out.WriteLine($"Win +{gain}. Bank: {bank}");
            }

            var possibleProfit = Math.Min(_rate10 * 10, _rate20 * 20);
            if (possibleProfit < _currentLoss)
            {
                _rate10 += 2;
                _rate20 += 1;
                @out.WriteLine($"raise to {_rate10} {_rate20}");
            }
        }
    }
}
